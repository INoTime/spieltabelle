"use strict";
exports.__esModule = true;
var bodyParser = require("body-parser");
var fs = require("fs");
var cors = require("cors");
var express = require('express');
// Allowed extensions list can be extended depending on your own needs
var allowedAPIs = [
    'teams',
    'matches'
];
// path to dir where matches and teams are saved
var fileDir = 'spieletabelle';
var whitelist = ['http://localhost:9090', 'http://localhost:9090/api/matches', 'http://localhost:9090/api/teams'];
var corsOptionsDelegate = function (req, callback) {
    var corsOptions;
    if (whitelist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
    }
    else {
        corsOptions = { origin: false }; // disable CORS for this request
    }
    callback(null, corsOptions); // callback expects two parameters: error and options
};
var Server = /** @class */ (function () {
    function Server() {
        var _this = this;
        this.port = 9090;
        // Create expressjs application
        this.app = express();
        // Depending on your own needs, this can be extended
        this.app.use(bodyParser.json({ limit: '50mb' }));
        this.app.use(bodyParser.raw({ limit: '50mb' }));
        this.app.use(bodyParser.text({ limit: '50mb' }));
        this.app.use(bodyParser.urlencoded({
            limit: '50mb',
            extended: true
        }));
        // Route our backend calls
        // Gibt die Teams zurück
        this.app.options('/api/teams/:year', cors(corsOptionsDelegate));
        this.app.get('/api/teams/:year', cors(corsOptionsDelegate), function (myRequest, myResponse) {
            var year = myRequest.params.year;
            if (Server.validateYear(year)) {
                var path_1 = fileDir + "/teams/" + myRequest.params.year + ".json";
                fs.readFile(path_1, function (err, data) {
                    if (err) {
                        myResponse.send('Fehler');
                        return console.log(err);
                    }
                    var body = JSON.parse(data);
                    myResponse.status(200).type('json').json(body);
                });
            }
            else {
                myResponse.status(400).json("'" + year + "' entspricht keinem Jahr");
            }
        });
        // Gibt alle Matches zurück
        this.app.options('/api/matches/:year', cors());
        this.app.get('/api/matches/:year', cors(), function (myRequest, myResponse) {
            var year = myRequest.params.year;
            if (Server.validateYear(year)) {
                var path_2 = fileDir + "/matches/" + myRequest.params.year + ".json";
                fs.readFile(path_2, function (err, data) {
                    if (err) {
                        return console.log(err);
                    }
                    var body = JSON.parse(data);
                    myResponse.json(body);
                });
            }
            else {
                myResponse.status(400).json("'" + year + "' entspricht keinem Jahr");
            }
        });
        this.app.options('/api/savefile', cors());
        this.app.post('/api/savefile', cors(), function (req, res) {
            var year = req.body['year'];
            var matches = req.body['matches'];
            console.log("year: " + year);
            console.log("matches: " + matches);
            if (Server.validateYear(year)) {
                fs.writeFile("seasons/" + year + ".json", JSON.stringify(matches), function (err) {
                    if (err) {
                        console.log(err);
                        res.status(501).send('Fehler beim speichern');
                    }
                    else {
                        res.status(200).send('{"result": "Saison gespeichert"}');
                    }
                });
            }
            else {
                res.status(400)
                    .json({
                    year: year,
                    matches: matches,
                    message: 'Fehlerhafte Werte'
                });
            }
        });
        this.app.options('/api/getAllSavedSeasons', cors());
        this.app.get('/api/getAllSavedSeasons', cors(), function (req, res) {
            fs.exists('seasons', function (isExists) {
                if (isExists) {
                    fs.readdir('seasons', function (err, files) {
                        if (err) {
                            console.log(err);
                            res.status(501).statusMessage = 'Fehler beim lesen';
                            res.send();
                        }
                        else {
                            var seasons = [];
                            for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
                                var filename = files_1[_i];
                                filename = filename.replace(".json", "");
                                seasons.push(filename);
                            }
                            res.status(200).json({ seasons: seasons });
                        }
                    });
                }
                else {
                    res.status(200).send([]);
                }
            });
        });
        this.app.options('/api/getSavedSeason/:year', cors());
        this.app.get('/api/getSavedSeason/:year', cors(), function (req, res) {
            console.log("load Data");
            var year = req.params['year'];
            try {
                fs.exists('./seasons/' + year + ".json", function (isExists) {
                    if (isExists) {
                        fs.readFile('./seasons/' + year + '.json', function (err, rawdata) {
                            console.log("" + rawdata);
                            res.status(200).send(rawdata);
                        });
                    }
                    else {
                        res.status(501).send('Saison nicht gefunden');
                    }
                });
            }
            catch (err) {
                console.log(err);
                res.status(501).send('Fehler beim laden');
            }
        });
        // Start the server on the provided port
        this.app.listen(this.port, function () { return console.log("http is started " + _this.port); });
    }
    Server.bootstrap = function () {
        return new Server();
    };
    Server.validateYear = function (year) {
        var regexp = new RegExp('^\\d{4}$');
        console.log(year);
        return regexp.test(year);
    };
    Server.validateSeason = function (season) {
        var result = (season && JSON.parse(season));
        // console.log(Object.keys(JSON.parse(season)).length);
        console.log(result);
        return result;
    };
    return Server;
}());
//Bootstrap the server, so it is actualy started
var server = Server.bootstrap();
exports["default"] = server.app;
