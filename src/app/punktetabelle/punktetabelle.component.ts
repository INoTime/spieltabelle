import { Component, OnInit } from '@angular/core';

import { BundesligaService } from '../services/bundesliga.service';
import { Tabelle } from '../datatypes/tabelle';
import { Team } from '../datatypes/team';
import { subscribeOn } from 'rxjs/operators';
import { MainComponent } from 'src/app/main/main.component';
import { SpieltagComponent } from '../spieltag/spieltag.component';
/**
 *
 */
@Component({
  selector: 'app-punktetabelle',
  templateUrl: './punktetabelle.component.html',
  styleUrls: ['./punktetabelle.component.css']
})
export class PunktetabelleComponent implements OnInit {
  public tabelle: Tabelle;
  public teams: Team[] = [];
  private theYear: MainComponent = new MainComponent();

  constructor(private service: BundesligaService) { }

  ngOnInit() {
    //* Die nachfolgende Zeile ist für das holen aller Teams
    this.service.getTeams(this.theYear.year.toString()).subscribe(res => this.teams = res);

    this.tabelle = this.service.tabelle;
    //* Denke daran das du noch die Spieltag Matches benötigst. (service Funktion)
  }

  selected2019() : void {
    this.theYear.year = 2019;
    this.service.refreshTeams(this.theYear.year).subscribe(teams => this.tabelle.teams = teams);
  }

  selected2020() : void {
    this.theYear.year = 2020;
    this.service.refreshTeams(this.theYear.year).subscribe(teams => this.tabelle.teams = teams);
  }
}
