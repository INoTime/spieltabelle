import { Injectable } from '@angular/core';
import { Team } from '../datatypes/team';
import { Match } from '../datatypes/match';

/**
 * Hier finden die Konvertierung statt, die bei der Kommunikation zwischen den Servern benötigt werden
 *
 * Diese Datei muss nicht weiter angepasst werden
 */
@Injectable({
  providedIn: 'root'
})
export class ConverterService {
  constructor() { }


  public convertJsonToTeams(teams: any[]): Team[] {
    let result: Team[] = [];
    for (let team of teams) {
      result.push({
        name: team.TeamName,
        shortname: team.ShortName,
        points: 0,
        goalsAgainst: 0,
        goalsFor: 0,
        diff: 0
      });
    }
    return result;
  }

  public convertJsonToMatches(matches: any): Match[] {

    let matchTemp: Match[] = [];
    let count = 0;
    for (let match of matches) {
      count++;
      if(count < 10){
      //if(matchresults.length > 0) {
        matchTemp.push({
          homeTeam: match.Team1.TeamName,
          awayTeam: match.Team2.TeamName,
          goalsAwayTeam: 0,//matchresults[1].PointsTeam1,
          goalsHomeTeam: 0, //matchresults[1].PointsTeam2,
          spieltag: match.Group.GroupOrderID
        });
      }
    }


    return matchTemp;
  }
}
