import { Component, OnInit } from '@angular/core';

import { Match } from 'src/app/datatypes/match';
import { Tabelle } from 'src/app/datatypes/tabelle';
import { BundesligaService } from 'src/app/services/bundesliga.service';
import { Team } from 'src/app/datatypes/team';
import { MainComponent } from 'src/app/main/main.component';

@Component({
  selector: 'app-spieltag',
  templateUrl: './spieltag.component.html',
  styleUrls: ['./spieltag.component.css']
})
export class SpieltagComponent implements OnInit {

  public matches: Match[] = [];
  private theYear: MainComponent = new MainComponent();

  constructor(private service: BundesligaService) { }
  ngOnInit() {
    this.service.getMatches(this.theYear.year.toString()).subscribe(res => this.matches = res);
  }

  berechnePunkte(): void {
    this.service.generateTabelle(this.matches);
  }

  // hier müssen noch die Methoden für das Laden und speichern hinzugefügt werden.
  // der Service bietet auch dafür Funktionen
  ladenSpeichernPunkte() : void {
    this.service.saveMatches(this.matches, this.theYear.year);
  }

  selected2019() : void {
    this.theYear.year = 2019;
    this.service.refreshMatches(this.theYear.year).subscribe(matches => this.matches = matches);
  }

  selected2020() : void {
    this.theYear.year = 2020;
    this.service.refreshMatches(this.theYear.year).subscribe(matches => this.matches = matches);
  }
}
